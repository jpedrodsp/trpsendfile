import sys
sys.path.append('src/trp')
import trp_client
import trp_server
import datetime
import argparse

default_split_size = 64

def segment_file(path, splitsize):
    segments = []
    with open(path, 'r') as file:
        data = file.read()
    i = 0
    split = []
    while i < len(data):
        split.append(data[i:i+splitsize])
        i += splitsize
    return split, len(data)


def join_array(split_array):
    joined_array = ''
    for i in split_array:
        joined_array += i
    return joined_array

class Server:
    file_buffer = []
    def __init__(self):
        self.file_buffer = []
        pass

class Client:
    i = 0
    filepath = None
    destination = None
    def __init__(self, file_to_send):
        self.filepath = file_to_send
    def set_destination(self, target_host, target_port):
        self.destination = (target_host, target_port)
    def start(self):
        if self.filepath and self.destination:
            # Open and split the file first
            splitted_file, size = segment_file(path=self.filepath, splitsize=default_split_size)
            print("File splitted.")
            # Create handler and handshake with server
            client_handler = trp_client.TRP_client()
            print("Sending handshake.")
            client_handler.send("!HANDSHAKE", target_host=self.destination[0], target_port=self.destination[1])
            self.i += 1
            # Send the splitted files via TRP, calculating the time spent
            print("Sending file.")
            initial_time = datetime.datetime.now()
            for piece in splitted_file:
                client_handler.send(data=piece, target_host=self.destination[0], target_port=self.destination[1])
            final_time = datetime.datetime.now()
            time_difference = final_time - initial_time
            print("Sent", size, "bytes in", time_difference.microseconds, "us")
            # Close connection
            client_handler.send(data="!END", target_host=self.destination[0], target_port=self.destination[1])
            print("End of file sending (client side).")

class Server:
    server = None
    output_filepath = 'file.out'
    def __init__(self, output_filepath):
        self.output_filepath = output_filepath
        pass
    def start(self, hostport):
        self.server = trp_server.TRP_server()
        # Wait for handshake
        print("Waiting for handshake...")
        filename = self.output_filepath
        while True:
            client_handshake = self.server.receive(hostport=hostport)
            if client_handshake:
                if client_handshake == '!HANDSHAKE':
                    print("Client handshaked with server sucessfully.")
                    break

        # Create file and make ready to buffer it on disk
        print("Creating file", filename, "...")
        file = open(filename, 'w')
        print("File created.")
        #final_received_data = ''
        print("Receiving data...")
        while True:
            client_data = self.server.receive(hostport=hostport)
            if client_data:
                #print(client_data)
                if client_data == '!END':
                    break
                else:
                    #final_received_data += client_data
                    file.write(client_data)
        file.close()
        #print(final_received_data)
        print("End of file sending (server side).")

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("mode", metavar="MODETYPE")
    parser.add_argument("filepath", metavar="CLIENT_FILEPATH")
    parser.add_argument("target_host", metavar="SERVER_HOST", default="localhost")
    parser.add_argument("target_port", metavar="SERVER_PORT", type=int, default=2550)
    args = parser.parse_args()

    if args.mode == "CLIENT":
        node = Client(args.filepath)
        node.set_destination(args.target_host, args.target_port)
        print("Sending", node.filepath, "to", str(node.destination[0]) + ':' + str(node.destination[1]))
        node.start()

    elif args.mode == "SERVER":
        node = Server(args.filepath)
        print("Waiting for file...")
        node.start(args.target_port)
        pass